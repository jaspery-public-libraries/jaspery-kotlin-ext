/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.testng.dataprovider

/**
 * Describes test scenario used by [dataProvider] builder function.
 *
 * @see dataProvider
 */
internal class Scenario private constructor(internal val params: List<Any>) {
    private constructor(builder: Builder) : this(builder.params)

    internal class Builder(args: Array<out Any>) {
        internal val params = args.toList()
        fun build() = Scenario(this)
    }
}
