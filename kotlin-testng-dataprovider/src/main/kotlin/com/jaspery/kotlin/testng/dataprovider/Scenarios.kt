/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.testng.dataprovider

/**
 * Describes sequence of test [Builder.scenario]s used by [dataProvider] builder function.
 *
 * @see dataProvider
 * @see testNGDataArray
 */
class Scenarios private constructor(private val scenarios: List<Scenario>) {
    private constructor (builder: Builder) : this(builder.scenarios)

    /**
     * Converts sequence of test scenarios into conventional TestNG data provider output.
     *
     * In TestNG data-provider is a method supplying data for a test method. Data-provider
     * method must return an Object[][] where each Object[] can be assigned the parameter
     * list of the test method.
     *
     * @return array of arrays of [Any] objects which represent scenario sequence data in
     * TestNG expected format (which is `Object[][]`).
     */
    fun testNGDataArray(): Array<Array<Any>> = scenarios.map { it.params.toTypedArray() }.toTypedArray()

    /**
     * Builder collects scenarios data and allows to create [Scenarios] object using [build] function.
     */
    class Builder {
        internal val scenarios = mutableListOf<Scenario>()

        /**
         * Adds scenario to builder internal collection, which later will be added into [Scenarios] object.
         *
         * @param args values for parameters of test function, supplied by this data provider.
         */
        fun scenario(vararg args: Any) {
            scenarios.add(Scenario.Builder(args).build())
        }

        /**
         * Builds [Scenarios] object from parameters added by [scenario] function.
         *
         * @return [Scenarios] object
         */
        fun build() = Scenarios(this)
    }
}
