/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.testng.dataprovider

/**
 * Introduces custom DSL for TestNG data providers. Example:
 * ```
 *  @DataProvider
 *  fun createRangeDataProvider(): Array<Array<Any>> {
 *      return dataProvider {
 *          scenario(LocalDate.parse("2017-01-01"), LocalDate.parse("2017-01-07"))
 *          scenario(LocalDate.parse("2017-04-05"), LocalDate.parse("2017-05-25"))
 *          scenario(LocalDate.parse("2017-05-25"), LocalDate.parse("2017-05-25"))
 *      }.testNGDataArray()
 *  }
 * ```
 *
 * Same example, more concise:
 * ```
 *  @DataProvider
 *  fun createRangeDataProvider() = dataProvider {
 *      scenario(LocalDate.parse("2017-01-01"), LocalDate.parse("2017-01-07"))
 *      scenario(LocalDate.parse("2017-04-05"), LocalDate.parse("2017-05-25"))
 *      scenario(LocalDate.parse("2017-05-25"), LocalDate.parse("2017-05-25"))
 *  }.testNGDataArray()
 *  ```
 *
 * Complete test-case example:
 *
 *  @sample TestNGDataProviderTest
 */
fun dataProvider(block: Scenarios.Builder.() -> Unit) = Scenarios.Builder().apply(block).build()
