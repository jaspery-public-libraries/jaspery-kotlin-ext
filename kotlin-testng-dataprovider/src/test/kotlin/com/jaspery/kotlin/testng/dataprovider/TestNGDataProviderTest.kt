/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.testng.dataprovider

import org.testng.Assert.assertEquals
import org.testng.Assert.fail
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

class TestNGDataProviderTest {
    @DataProvider
    fun dataProviderTestNGDSL() = dataProvider {
        scenario(1, "I like to run", Pair("key", 2.5))
    }.testNGDataArray()

    @Test(dataProvider = "dataProviderTestNGDSL")
    fun `test data-provider DSL`(num: Int, str: String, any: Any) {
        assertEquals(num, 1)
        assertEquals(str, "I like to run")
        if (any is Pair<*, *>) {
            assertEquals(any.first, "key")
            assertEquals(any.second, 2.5)
        } else {
            fail("any should be instance of Pair, but is $any")
        }
    }
}