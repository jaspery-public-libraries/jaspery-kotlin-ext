## Jaspery Kotlin Ext

> Set of useful Kotlin micro-utilities.

## Modules

* [jaspery-kotlin-lang](#jaspery-kotlin-lang)
Convenient precondition checking function for Kotlin
* [kotlin-testng-dataprovider](#kotlin-testng-dataprovider)
Simple Kotlin-DSL-based builder for TestNG data-providers
* [threetenbp-ext](#threeten-backports-extension)
Few extension classes and functions for threetenbp library
* [jacoco-build-utils](#jacoco-build-utils)
Utility function to extract code coverage from Jacoco XML report

## Installing / Getting started

Libraries are published via [JitPack](https://jitpack.io).
To use them from Gradle add <https://jitpack.io> to repositories list like this:

```gradle
repositories {
    maven { url = 'https://jitpack.io' }
}
```

E.g., add dependency to `jaspery-kotlin-lang`:

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jaspery-kotlin-lang:-SNAPSHOT'
}
```

Artifact names for each module

| Module                     | Artifact Name
|:---------------------------|:---------------
| jaspery-kotlin-lang        | `com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jaspery-kotlin-lang:-SNAPSHOT`
| kotlin-testng-dataprovider | `com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:kotlin-testng-dataprovider:-SNAPSHOT`
| threetenbp-ext             | `com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:threetenbp-ext:-SNAPSHOT`
| jacoco-build-util          | `com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jacoco-build-utils:-SNAPSHOT`

There are no stable releases of these libraries yet, `-SNAPSHOT` is jitpack's conventional name for latest version.

**Note** about using <https://jitpack.io>:

1. As described in [this blog post](https://blog.autsoft.hu/a-confusing-dependency/), it's better
to list jitpack first in project's repositories
2. Consider using, Gradle API to declare what a repository may or may not contain.
[There are different use cases for it:](https://docs.gradle.org/5.1/userguide/declaring_repositories.html#sec::matching_repositories_to_dependencies)

## Jaspery Kotlin Lang

### Installing

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jaspery-kotlin-lang:-SNAPSHOT'
}
```

### Quick Start

```kotlin
import com.jaspery.kotlin.lang.requireValueIn

private fun requireYearInRange(year: Int): Int {
    return requireValueIn(year, 1900..2100) {
        "Year not supported $year"
    }
}
```

### Features

For now it contains few requireXxx type functions which are useful to check preconditions.

### API Documentation

AI: Add here link to KDoc

### Kotlin TestNG DataProvider

### Installing

Typically `kotlin-testng-dataprovider` is added to test scope using `testImplementation`:

```gradle
dependencies {
    testImplementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:kotlin-testng-dataprovider:-SNAPSHOT'
}
```

### Features

* Simple Kotlin-DSL-based builder for TestNG data-providers

TestNG requires data provider method to return an `Object[][]` where each `Object[]`
can be assigned the parameter list of the test method. `dataProvider` builder provides
more readable notation to provide data for test methods.

### Quick Start

```kotlin
 @DataProvider
 fun createRangeDataProvider() = dataProvider {
     scenario(10, "2017-01-07")
     scenario(15, "2017-05-25")
     scenario(20, "2017-05-25")
 }.testNGDataArray()
```

### API Documentation

AI: Add here link to KDoc

## ThreeTen Backports Extension

### Installing

```gradle
dependencies {
    implementation 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:threetenbp-ext:-SNAPSHOT'
}
```

### Features

* LocalDateRange - class which allows to work with date ranges described with inclusive start and end date
* MonthDayRange - class which allows to work with month-day ranges described with inclusive start and
end month-days. It represents date range without year, time or time-zone information.
* Various utility functions to create, convert, process LocalDateRange, MonthDateRange and other objects
from threeten-bp library

### Quick Start

Create date object

```kotlin
val dt = "2018-01-15".date
```

Create year object

```kotlin
val y = 2019.year
```

Check date in range

```kotlin
if ("2018-01-15".date in LocalDateRange("2018-01-10".date, "2018-01-25".date)) {
    ...
}
```
See more examples in [API Documentation](#api-documentation-2)

### API Documentation

AI: Add here link to KDoc

## Jacoco Build Utils

### Installing

It is usually installed into build script classpath:

```gradle
buildscript {
    repositories {
        maven { url = 'https://jitpack.io' }
    }
    dependencies {
        classpath 'com.gitlab.jaspery-public-libraries.jaspery-kotlin-ext:jacoco-build-utils:-SNAPSHOT'
    }
}
```

### Features

It allows to extract test coverage data from jacoco xml report using
`CodeCoverageUtils.extractCodeCoverageFromXmlReport` function.

### Quick Start

Extract code coverage data and print it to build log, so it's later can be used
by GitLab or other CI/CD tool to display test coverage in job, or for similar purpose.

E.g. see GitLab documentation:
<https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-parsing>

Example how to use it in Gradle build script:

```gradle
task jacocoAcceptanceTestAggregateReport(type: JacocoReport) {
    doLast {
        def xmlReport = jacocoAcceptanceTestAggregateReport.reports.xml.destination
        def coverage = CodeCoverageUtils.extractCodeCoverageFromXmlReport(xmlReport)
        println("Acceptance Test Coverage: " + (100 * coverage).round() + "%")
    }
}
```

### API Documentation

AI: Add here link to KDoc

## Deployment / Publishing

Code is hosted on [GitLab](https://gitlab.com/jaspery-public-libraries/jaspery-kotlin-ext)
and published via [JitPack](https://jitpack.io)

## Links

- Project homepage: [https://jaspery-public-libraries.gitlab.io] (Under Construction)
- Repository: [https://gitlab.com/jaspery-public-libraries/jaspery-kotlin-ext]
- Related projects:
  - Other project in [jaspery-public-libraries group](https://gitlab.com/jaspery-public-libraries)

## Built With

* Kotlin 1.3 <https://kotlinlang.org>
* Gradle 5.1 <https://gradle.org>
* GitLab <https://gitlab.com>
* JitPack <https://jitpack.io>
* Libraries
  * ThreeTenBP <https://www.threeten.org/threetenbp/>
* Libraries - test
  * TestNG <https://testng.org>
  * MockK <https://mockk.io>
  * AssertJ <http://joel-costigliola.github.io/assertj/>
  * JaCoCo <https://www.eclemma.org/jacoco/>

## Licensing

Pre-release (snapshot) code in this project is licensed under GPL v3.0 license.
This may change in the future.

## About Author

Copyright &copy; 2018. Ihor Halanyuk <https://gitlab.com/jaspery-public-libraries>
