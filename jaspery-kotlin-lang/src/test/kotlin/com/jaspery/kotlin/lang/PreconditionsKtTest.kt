/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.kotlin.lang

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.testng.annotations.DataProvider
import org.testng.annotations.Test

internal class PreconditionsKtTest {
    @Test(dataProvider = "dataProvider1")
    fun testRequireFail(value: Any) {
        assertThatThrownBy { require(value) { _ -> false } }
                .isInstanceOf(IllegalArgumentException::class.java)
                .withFailMessage("Failed requirement.")
    }

    @Test(dataProvider = "dataProvider1")
    fun testRequirePass(value: Any) {
        assertThat(require(value) { _ -> true }).isEqualTo(value)
    }

    @Test
    fun testRequireWithMessage() {
    }

    @Test(dataProvider = "dataProvider2")
    fun testRequireValueIn(value: Comparable<Any>, range: ClosedRange<Comparable<Any>>) {
        if (value in range) {
            assertThat(requireValueIn(value, range)).isEqualTo(value)
        } else {
            assertThatThrownBy { requireValueIn(value, range) }
                    .isInstanceOf(IllegalArgumentException::class.java)
                    .withFailMessage("Failed requirement.")
        }
    }

    @Test(dataProvider = "dataProvider2")
    fun testRequireValueInWithMessage(value: Comparable<Any>, range: ClosedRange<Comparable<Any>>) {
        if (value in range) {
            assertThat(requireValueIn(value, range) { "Custom message $value" }).isEqualTo(value)
        } else {
            assertThatThrownBy { requireValueIn(value, range) { "Custom message $value" } }
                    .isInstanceOf(IllegalArgumentException::class.java)
                    .hasMessageStartingWith("Custom message")
        }
    }

    @DataProvider
    fun dataProvider1() = dataProvider {
        scenario(1)
    }.testNGDataArray()

    @DataProvider
    fun dataProvider2() = dataProvider {
        scenario(5, 1..10)
        scenario(1, 5..10)
    }.testNGDataArray()
}