/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.threetenbp.ext

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.Month
import org.threeten.bp.MonthDay

class MonthDayRangeTest {

    @Test(dataProvider = "testParseProviderValid")
    fun testParse(string: String, result: MonthDayRange) {
        assertThat(MonthDayRange.parse(string)).isEqualTo(result)
    }

    @Test(dataProvider = "testParseOneDayProviderValid")
    fun testParseOneDay(string: String, result: MonthDayRange) {
        assertThat(MonthDayRange.parse(string)).isEqualTo(result)
    }

    @Test(dataProvider = "testParseInvalid")
    fun testParseInvalid(string: String) {
        assertThatIllegalArgumentException().isThrownBy {
            MonthDayRange.parse(string)
        }.withMessageStartingWith("Cannot parse")
    }

    @Test
    fun testParseEmpty() {
        assertThatIllegalArgumentException().isThrownBy {
            MonthDayRange.parse("")
        }.withMessageStartingWith("Cannot parse")
    }

    @Test
    fun testStartAfterEnd() {
        assertThatIllegalArgumentException().isThrownBy {
            MonthDayRange(5, 1, Month.NOVEMBER)
        }.withMessageStartingWith("End date in range must be not before the start date:")
    }

    @Test
    fun testOneDay() {
        val monthDayRange = MonthDayRange(MonthDay.of(Month.NOVEMBER, 15))
        assertThat(monthDayRange.endInclusive).isEqualTo(monthDayRange.startInclusive)
    }

    @Test(dataProvider = "monthDaysInRange")
    fun `test contains positive`(date: MonthDay, range: MonthDayRange) {
        assertThat(date in range).isTrue()
    }

    @Test(dataProvider = "monthDaysNotInRange")
    fun `test contains negative`(date: MonthDay, range: MonthDayRange) {
        assertThat(date in range).isFalse()
    }

    @Test(dataProvider = "testParseProviderValid")
    fun testToStringFull(string: String, range: MonthDayRange) {
        assertThat(range.toString()).isEqualTo(string)
    }

    @Test(dataProvider = "testParseOneDayProviderValid")
    fun testToStringOneDay(string: String, range: MonthDayRange) {
        assertThat(range.toString()).isEqualTo(string)
    }

    @DataProvider
    fun testParseProviderValid() = dataProvider {
        scenario("20.11-26.11", KING_UNIVERSE_RANGE)
        scenario("22.03-25.04", EASTER_RANGE)
        scenario("1.01-22.01", MonthDayRange(1, 22, Month.JANUARY))
    }.testNGDataArray()

    @DataProvider
    fun testParseOneDayProviderValid() = dataProvider {
        scenario("25.12", CHRISTMAS_RANGE)
        scenario("1.11", ALL_SAINTS_RANGE)
    }.testNGDataArray()

    @DataProvider
    fun testParseInvalid() = dataProvider {
        scenario("1.01-22.01-25.02")
        scenario("1.02-31.02")
        scenario("I like to run, out in the sun")
    }.testNGDataArray()

    @DataProvider
    fun monthDaysInRange() = dataProvider {
        scenario(MonthDay.of(Month.NOVEMBER, 21), KING_UNIVERSE_RANGE)
        scenario(MonthDay.of(Month.APRIL, 1), EASTER_RANGE)
        scenario(CHRISTMAS, CHRISTMAS_RANGE)
    }.testNGDataArray()

    @DataProvider
    fun monthDaysNotInRange() = dataProvider {
        scenario(MonthDay.of(Month.NOVEMBER, 19), KING_UNIVERSE_RANGE)
        scenario(MonthDay.of(Month.APRIL, 26), EASTER_RANGE)
        scenario(MonthDay.of(Month.NOVEMBER, 3), ALL_SAINTS_RANGE)
    }.testNGDataArray()

    companion object {
        private val EASTER_EARLIEST = MonthDay.of(Month.MARCH, 22)
        private val EASTER_LATEST = MonthDay.of(Month.APRIL, 25)
        val EASTER_RANGE = MonthDayRange(EASTER_EARLIEST, EASTER_LATEST)

        val KING_UNIVERSE_RANGE = MonthDayRange(20, 26, Month.NOVEMBER)

        val CHRISTMAS: MonthDay = MonthDay.of(Month.DECEMBER, 25)
        val CHRISTMAS_RANGE = MonthDayRange(CHRISTMAS)

        private val ALL_SAINTS: MonthDay = MonthDay.of(Month.NOVEMBER, 1)
        val ALL_SAINTS_RANGE = MonthDayRange(ALL_SAINTS)
    }
}
