/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.threetenbp.ext

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatIllegalArgumentException
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.LocalDate

internal class LocalDateRangeTest {
    @Test(dataProvider = "createRangeDataProvider")
    fun `test create range`(startIncl: LocalDate, endIncl: LocalDate) {
        val dateRange = LocalDateRange(startIncl, endIncl)
        assertThat(dateRange.startInclusive).isEqualTo(startIncl)
        assertThat(dateRange.endInclusive).isEqualTo(endIncl)
    }

    @Test(dataProvider = "createRangeFixDateDataProvider")
    fun `test create range fix date`(fixDate: LocalDate) {
        val dateRange = LocalDateRange(fixDate)
        assertThat(dateRange.startInclusive)
                .isEqualTo(dateRange.endInclusive)
                .isEqualTo(fixDate)
    }

    @Test(dataProvider = "createInvalidRangeDataProvider")
    fun `test create invalid range`(startIncl: LocalDate, endIncl: LocalDate) {
        assertThatIllegalArgumentException().isThrownBy {
            LocalDateRange(startIncl, endIncl)
        }.withMessageStartingWith("End date in range must be not before the start date:")
    }

    @Test(dataProvider = "dateInRange")
    fun `test contains`(date: LocalDate, range: LocalDateRange, inRange: Boolean) {
        assertThat(date in range).isEqualTo(inRange)
    }

    @DataProvider
    fun dateInRange() = dataProvider {
        scenario("2017-01-02".date, LocalDateRange("2017-01-01".date, "2017-01-07".date), true)
        scenario("2017-04-04".date, LocalDateRange("2017-04-05".date, "2017-05-25".date), false)
    }.testNGDataArray()

    @DataProvider
    fun createRangeDataProvider() = dataProvider {
        scenario("2017-01-01".date, "2017-01-07".date)
        scenario("2017-04-05".date, "2017-05-25".date)
        scenario("2017-05-25".date, "2017-05-25".date)
    }.testNGDataArray()

    @DataProvider
    fun createInvalidRangeDataProvider() = dataProvider {
        scenario("2017-01-15".date, "2017-01-07".date)
    }.testNGDataArray()

    @DataProvider
    fun createRangeFixDateDataProvider() = dataProvider {
        scenario("2017-05-25".date)
    }.testNGDataArray()
}

