/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.threetenbp.ext

import com.jaspery.kotlin.testng.dataprovider.dataProvider
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.testng.annotations.DataProvider
import org.testng.annotations.Test
import org.threeten.bp.DateTimeException
import org.threeten.bp.LocalDate
import org.threeten.bp.Month.*
import org.threeten.bp.MonthDay
import org.threeten.bp.Year
import org.threeten.bp.format.DateTimeParseException

internal class ThreeTenBpTest {
    @Test
    fun `test parse year happy`() {
        assertThat(2018.year).isEqualTo(Year.of(2018))
    }

    @Test
    fun `test parse year unhappy`() {
        assertThatThrownBy {
            1000000000.year
        }.isInstanceOf(DateTimeException::class.java)
    }

    @Test
    fun `test year minus operator happy`() {
        assertThat(2018.year - 15).isEqualTo(2003.year)
    }

    @Test
    fun `test year plus operator happy`() {
        assertThat(2018.year + 15).isEqualTo(2033.year)
    }

    @Test
    fun `test year inc operator happy`() {
        var year = 2018.year
        year++
        assertThat(year).isEqualTo(2019.year)
    }

    @Test
    fun `test year minus operator unhappy`() {
        val year = (Year.MIN_VALUE).year
        assertThatThrownBy {
            year - 15
        }.isInstanceOf(DateTimeException::class.java)
                .withFailMessage("Invalid int value for Year: -1000000014")
    }

    @Test
    fun `test parse date happy`() {
        val date = "2018-06-12".date
        assertThat(date).isEqualTo(LocalDate.of(2018, JUNE, 12))
    }

    @Test
    fun `test parse date unhappy`() {
        assertThatThrownBy {
            "asd".date
        }.isInstanceOf(DateTimeException::class.java)
    }

    @DataProvider
    fun monthDayParseFormatHappyDataProvider() = dataProvider {
        scenario("1.12", MonthDay.of(DECEMBER, 1))
        scenario("05.11", MonthDay.of(NOVEMBER, 5))
        scenario("29.02", MonthDay.of(FEBRUARY, 29))
    }.testNGDataArray()

    @Test(dataProvider = "monthDayParseFormatHappyDataProvider")
    fun `test parse MonthDay happy`(monthDayString: String, monthDay: MonthDay) {
        assertThat(monthDayString.monthDay).isEqualTo(monthDay)
    }

    @Test(dataProvider = "monthDayParseFormatHappyDataProvider")
    fun `test format MonthDay happy`(monthDayString: String, monthDay: MonthDay) {
        assertThat(monthDay.formatDayDotMonth()).isEqualTo(monthDayString.trimStart('0'))
    }

    @Test
    fun `test parse MonthDay unhappy`() {
        assertThatThrownBy {
            "32.12".monthDay
        }.isInstanceOf(DateTimeParseException::class.java)

        assertThatThrownBy {
            "25-05".monthDay
        }.isInstanceOf(DateTimeParseException::class.java)

        assertThatThrownBy {
            "--21-04".monthDay
        }.isInstanceOf(DateTimeParseException::class.java)
    }

    @Test
    fun `test MonthDay to LocalDate conversion`() {
        assertThat(
                "24.12".monthDay.atYear(2018.year)
        ).isEqualTo(
                "2018-12-24".date
        )
    }

    @Test
    fun `test LocalDate to MonthDay conversion`() {
        assertThat(
                "2018-11-27".date.monthDay
        ).isEqualTo(
                "27.11".monthDay
        )
    }

    @Test
    fun `test impossible MonthDay to LocalDate conversion`() {
        assertThat(
                "29.02".monthDay.atYear(2018.year)
        ).isEqualTo(
                "2018-02-28".date
        )
    }
}
