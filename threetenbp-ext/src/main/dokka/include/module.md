# Module threetenbp-ext

The module contains few useful data classes and extension functions which extend
functionality of [ThreeTen Backport library](https://www.threeten.org/threetenbp/).

See [com.jaspery.threetenbp.ext] package for more details.
