/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 *
 */

package com.jaspery.threetenbp.ext

import org.threeten.bp.Month
import org.threeten.bp.MonthDay
import org.threeten.bp.format.DateTimeParseException

/**
 * Month-day range represents date range without specifying year.
 *
 * This is an immutable representation for date range without year, time or time-zone information.
 *
 * Examples of month day ranges are
 * - 5-Jan to 11-Jan
 * - 10-Jan to 15-Mar
 * - etc
 *
 * **Limitation**: semantically it is not possible to represent month-day range which spans beginning
 * of a year. Thus, date range *25-Dec to 6-Jan* would be invalid. Such cases need to be handled
 * properly in client code.
 *
 * @property startInclusive start [month-day][MonthDay] of this range (inclusive)
 * @property endInclusive end [month-day][MonthDay] of this range (inclusive)
 *
 * @constructor Create [MonthDayRange] object from [start][startInclusive] and [end][endInclusive]
 * dates (both inclusive).
 *
 * @param startInclusive start month-day for this range (inclusive)
 * @param endInclusive end month-day for this range (inclusive)
 *
 * @throws IllegalArgumentException if [endInclusive] is before [startInclusive]
 */
data class MonthDayRange(val startInclusive: MonthDay, val endInclusive: MonthDay) {
    init {
        require(!startInclusive.isAfter(endInclusive)) {
            "End date in range must be not before the start date: ($startInclusive, $endInclusive "
        }
    }

    private val toStringCache by lazy {
        if (startInclusive == endInclusive) startInclusive.formatDayDotMonth()
        else "${startInclusive.formatDayDotMonth()}-${endInclusive.formatDayDotMonth()}"
    }

    /**
     * @constructor
     * Convenient constructor to create month-day range which does not span month start/end.
     *
     * @param startInclusive first day of range in [month]
     * @param endInclusive last day of range in [month]
     * @param month month-day range's month
     */
    constructor(startInclusive: Int, endInclusive: Int, month: Month)
            : this(MonthDay.of(month, startInclusive), MonthDay.of(month, endInclusive))

    /**
     * @constructor
     * Create [MonthDayRange] which represents one day ([startInclusive] == [endInclusive]).
     *
     * @param fixedMonthDay start and end month-day for one-day range
     */
    constructor(fixedMonthDay: MonthDay) : this(fixedMonthDay, fixedMonthDay)

    /**
     * '`In`' operator which allows to check if given [month-day][MonthDay] belongs to [this range][MonthDayRange].
     *
     * Both [startInclusive] and [endInclusive] always belong to [this range][LocalDateRange].
     *
     * Example usage:
     * ```
     * val monthDay: MonthDay = ...
     * val range: MonthDayRange = ...
     * val inRange: Boolean = date in range
     * ```
     */
    operator fun contains(monthDay: MonthDay): Boolean = !(monthDay.isBefore(startInclusive) || monthDay.isAfter(endInclusive))

    /**
     * Convenient to-string representation in form of *d.MM-d.MM* or *d.MM* for one-day range.
     *
     * This function is symmetrical with [parse], which means it produces output which can be parsed.
     */
    override fun toString(): String = toStringCache

    companion object {
        private const val ERROR_MESSAGE = "Cannot parse \"%s\" as MonthDayRange"

        /**
         * Parse [MonthDayRange] from string of format *d.MM-d.MM* or just *d.MM* for one-day range.
         *
         * Following values can be parsed by this function:
         * - 1.01-15.02
         * - 01.01-15.01
         * - 14.02
         *
         * @param mdRangeString conventional string representation of month-day range
         *
         * @throws IllegalArgumentException if [mdRangeString] cannot be parsed to [MonthDayRange]
         */
        fun parse(mdRangeString: String): MonthDayRange {
            val list = try {
                mdRangeString.split('-').map { it.monthDay }
            } catch (e: DateTimeParseException) {
                throw IllegalArgumentException(ERROR_MESSAGE.format(mdRangeString), e)
            }
            return when (list.size) {
                1 -> MonthDayRange(list.single())
                2 -> MonthDayRange(list[0], list[1])
                else -> throw IllegalArgumentException(ERROR_MESSAGE.format(mdRangeString))
            }
        }
    }
}
