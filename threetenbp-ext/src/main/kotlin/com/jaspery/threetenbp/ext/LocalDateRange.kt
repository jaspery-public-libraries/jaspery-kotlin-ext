/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 *
 */

package com.jaspery.threetenbp.ext

import org.threeten.bp.LocalDate

/**
 * Date range represented by [start][startInclusive] and [end][endInclusive] [LocalDate]s.
 *
 * This is an immutable representation for date range without time-zone information.
 *
 * @property startInclusive start date of this range (inclusive)
 * @property endInclusive end date of this range (inclusive)
 *
 * @constructor Create [LocalDateRange] object from [start][startInclusive] and [end][endInclusive]
 * dates (both inclusive).
 *
 * @param startInclusive start date for this range (inclusive)
 * @param endInclusive end date for this range (inclusive)
 *
 * @throws IllegalArgumentException if specified [end date][endInclusive] is before [start date][startInclusive]
 */
data class LocalDateRange(val startInclusive: LocalDate, val endInclusive: LocalDate) {
    init {
        require(!startInclusive.isAfter(endInclusive)) {
            "End date in range must be not before the start date: ($startInclusive, $endInclusive)"
        }
    }

    /**
     * Create [LocalDateRange] which represents one day ([startInclusive] == [endInclusive]).
     *
     * @param fixedDate start and end date for one-day range
     */
    constructor(fixedDate: LocalDate) : this(fixedDate, fixedDate)

    /**
     * '`In`' operator which allows to check if given [date][LocalDate] belongs to [this range][LocalDateRange].
     *
     * Both [startInclusive] and [endInclusive] always belong to [this range][LocalDateRange].
     *
     * Example usage:
     * ```
     * val date: LocalDate = ...
     * val range: LocalDateRange = ...
     * val inRange: Boolean = date in range
     * ```
     */
    operator fun contains(date: LocalDate): Boolean = !(date.isBefore(startInclusive) || date.isAfter(endInclusive))
}
