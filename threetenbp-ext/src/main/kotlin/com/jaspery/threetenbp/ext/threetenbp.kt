/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 *
 */

package com.jaspery.threetenbp.ext

import org.threeten.bp.LocalDate
import org.threeten.bp.MonthDay
import org.threeten.bp.Year
import org.threeten.bp.format.DateTimeFormatterBuilder
import org.threeten.bp.format.SignStyle
import org.threeten.bp.temporal.ChronoField

/**
 * '`+`' operator for [Year] - returns a copy of [this] year with the specified [number of years][increment] added.
 *
 * @param increment the years to add, may be negative
 * @return [Year] based on [this] with the [increment] added
 */
operator fun Year.plus(increment: Long): Year = this.plusYears(increment)

/**
 * '`-`' operator for [Year] - returns a copy of [this] year with the specified [number of years][decrement] subtracted.
 *
 * @param decrement the years to subtract, may be negative
 * @return [Year] based on [this] with the [decrement] subtracted
 */
operator fun Year.minus(decrement: Long): Year = this.minusYears(decrement)

/**
 * '`++`' operator for [Year] - returns a copy of [this] year with 1 year added.
 *
 * @return [Year] based on [this] with 1 year added
 */
operator fun Year.inc() = this + 1

/**
 * Convenient extension property to represent [Number] as [Year]. Example: `val y: Year = 1951.year`
 *
 * @return [Year] which represents [this number][Number] numeric value
 */
val Number.year: Year get() = Year.of(this.toInt())

/**
 * Convenient extension property to parse [String] as [LocalDate]. Example: `val d: LocalDate = "1951-03-25".date`
 *
 * @receiver must be non-empty string which can be parsed as [LocalDate]
 * @return [LocalDate] represented by [this string][String]
 */
val String.date: LocalDate get() = LocalDate.parse(this)

/**
 * Parse [MonthDay] from text like '1.12' or '18.11'.
 *
 * Example:
 * ```
 *  val md: MonthDay = "1.12".monthDay
 * ```
 * @receiver must be non-empty string which can be parsed as [MonthDay]
 * @return [MonthDay] represented by [this string][String]
 */
val String.monthDay: MonthDay get() = parseDayDotMonth(this)

/**
 * Extract [MonthDay] from [LocalDate].
 *
 * @return [MonthDay] which represents given [LocalDate] with year removed.
 */
val LocalDate.monthDay: MonthDay get() = MonthDay.from(this)

/**
 * Get [LocalDate] from [MonthDay] at given [year].
 *
 * @param year year to relate this [MonthDay] to
 * @return [local date][LocalDate] which is combination of [this] [MonthDay] and given [year]
 */
fun MonthDay.atYear(year: Year): LocalDate = this.atYear(year.value)

/**
 * Parse [MonthDay] from text like '1.12' or '18.11' (also support e.g. '01.05').
 *
 * @param mdString string to parse as [MonthDay]
 * @return [MonthDay] parsed from [mdString]
 */
fun parseDayDotMonth(mdString: CharSequence): MonthDay = MonthDay.parse(mdString, PARSER_DAY_DOT_MONTH)

/**
 * Format [this] [MonthDay] as 'd.MM'.
 */
fun MonthDay.formatDayDotMonth(): String = PARSER_DAY_DOT_MONTH.format(this)

private val PARSER_DAY_DOT_MONTH = DateTimeFormatterBuilder()
        .appendValue(ChronoField.DAY_OF_MONTH, 1, 2, SignStyle.NOT_NEGATIVE)
        .appendLiteral('.')
        .appendValue(ChronoField.MONTH_OF_YEAR, 2)
        .toFormatter()
