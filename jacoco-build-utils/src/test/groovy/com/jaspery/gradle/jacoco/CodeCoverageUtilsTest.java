/*
 * Copyright © 2018. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.gradle.jacoco;

import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URL;

public class CodeCoverageUtilsTest {

    @Test
    public void testExtractCodeCoverageFromXmlReport() {
        URL resource = CodeCoverageUtilsTest.class.getResource("/jacocoTestReport.xml");
        File f = new File(resource.getFile());
        double extractedCoverage = CodeCoverageUtils.extractCodeCoverageFromXmlReport(f);
        int roundedCoverage = (int) Math.round(100 * extractedCoverage);

        Assertions.assertThat(roundedCoverage).isEqualTo(44);
    }
}